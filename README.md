# TrueLayer Press

## Init progect

* Clone repo
* launch npm install

__________________________________

## Gulp task

* gulp serve (serve project for development)
* gulp build (bundle project on dist folder for production)

__________________________________

## Locale

If you need some locale translate, use json inside 'locale' folder.
Gulpfile.js read json based on

	const localePath = './app/locale/'+ lang +'.json';

Data is loaded into pug view, where you can simply use base interpolation to print out values

__________________________________

## Version / cache

Change version attribute on package.json to control version of bundle assets.
Bundle assets (css/js) will be renamed like this:

 name-lang_version.js
 name-lang_version.css

See function html()

__________________________________

## What's in there?

###Pug [website](https://pugjs.org)

HTML templating engine

__________________________________

### Sass [website](https://sass-lang.com/)

CSS extension language

__________________________________

### Lazysizes [github](https://github.com/aFarkas/lazysizes)

A lazy loader for resources (images, iframes, etc...)

__________________________________

## Ready to use?

If need below plugins/libraries ready to use:

### Animejs [github](https://github.com/juliangarnier/anime)

JavaScript animation engine

__________________________________

### Swiperjs [github](https://github.com/nolimits4web/swiper)

Most modern mobile touch slider with hardware accelerated transitions


