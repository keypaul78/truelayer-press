/*
** ========================================================================
**  General Utils functions/methods
** ========================================================================

** ========================================================================
*/

_TrueLYR.Utils = (function (_TrueLYR){
	'use strict';
	
	const CONFIG = {
		body: document.querySelector('body'),
		IE: 0
	};

	//check for IE
	const detectIE = function() {
		let ua = window.navigator.userAgent;
		let msie = ua.indexOf('MSIE ');
		if (msie > 0) {
			// IE 10 or older => return version number
			return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
		}
		let trident = ua.indexOf('Trident/');
			if (trident > 0) {
			// IE 11 => return version number
			let rv = ua.indexOf('rv:');
			return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
		}
		let edge = ua.indexOf('Edge/');
			if (edge > 0) {
			// Edge (IE 12+) => return version number
			return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
		}
		// other browser
		return false;
	};

	const detectIOS = function(){
		let userAgent = window.navigator.userAgent;
		if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)) {
		   return 'ios';
		}
		return false;
	};


	//check touches
	const isTouchDevice = function () {
		return !!('ontouchstart' in window);
	};

	const randomIntFromInterval = function(min, max) { // min and max included 
	  return Math.floor(Math.random() * (max - min + 1) + min);
	}

	const isInViewport = function(element) {
		
	  let rect = element.getBoundingClientRect();
	  let html = document.documentElement;
	  return (
	    rect.top >= 0 &&
	    rect.left >= 0 &&
	    rect.bottom <= (window.innerHeight || html.clientHeight) &&
	    rect.right <= (window.innerWidth || html.clientWidth)
	  );
	 
	};

	const isInViewportY = function(element) {
	
	  let rect = element.getBoundingClientRect();
	  let html = document.documentElement;
	  return (
	    rect.top >= 0 &&
	    rect.bottom <= (window.innerHeight || html.clientHeight)
	  );
	 	
	};
	
	const isBackInViewportY = function(element) {
	
	  let rect = element.getBoundingClientRect();
	  let html = document.documentElement;
	  return (
	    rect.top >= -rect.height &&
	    rect.bottom <= (window.innerHeight + rect.height || html.clientHeight + rect.height)
	  );
	 	
	};

	const getOffset = function( el ) {
	    let _x = 0;
	    let _y = 0;
	    let _w = 0;
	    let _h = 0;
	    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
	        _x += el.offsetLeft - el.scrollLeft;
	        _y += el.offsetTop - el.scrollTop;
	        _w = getElW(el);
	        el = el.offsetParent;
	    }
	    return { top: _y, left: _x, right: (_x + _w) };
	}

	const getRect = function(element) {
	  let rect = element.getBoundingClientRect();
	  return rect;
	};

	const getElH = function(element) {
		let rect = getRect(element);
		return rect.bottom - rect.top;
	};

	const getElW = function(element) {
		let rect = getRect(element);
		return rect.right - rect.left;
	};

	const isHidden = function(el) {
	    var style = window.getComputedStyle(el);
	    return ((style.display === 'none') || (style.visibility === 'hidden'))
	}

	/**
	 * Gets computed translate values
	 * @param {HTMLElement} element
	 * @returns {Object}
	 */
	function getTranslateValues (element) {
	  const style = window.getComputedStyle(element)
	  const matrix = style.transform || style.webkitTransform || style.mozTransform

	  // No transform property. Simply return 0 values.
	  if (matrix === 'none') {
	    return {
	      x: 0,
	      y: 0,
	      z: 0
	    }
	  }

	  // Can either be 2d or 3d transform
	  const matrixType = matrix.includes('3d') ? '3d' : '2d'
	  const matrixValues = matrix.match(/matrix.*\((.+)\)/)[1].split(', ')

	  // 2d matrices have 6 values
	  // Last 2 values are X and Y.
	  // 2d matrices does not have Z value.
	  if (matrixType === '2d') {
	    return {
	      x: matrixValues[4],
	      y: matrixValues[5],
	      z: 0
	    }
	  }

	  // 3d matrices have 16 values
	  // The 13th, 14th, and 15th values are X, Y, and Z
	  if (matrixType === '3d') {
	    return {
	      x: matrixValues[12],
	      y: matrixValues[13],
	      z: matrixValues[14]
	    }
	  }
	}

	const getViewportSize = function(){
		let w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		let h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
		let viewportSize = {
			w : w,
			h: h
		}
		return viewportSize;
	};

	const getComputedStyle = function(el){
		let style = el.currentStyle || window.getComputedStyle(el);
		return style;
	}

	const findAncestorByClass = function(el, cls) {
	    while ((el = el.parentElement) && !el.classList.contains(cls));
    	return el;
	}

	const stripPx = function(string){
		return string.replace('px','');
	};

	const totalPageH = function(){
		return (document.height !== undefined) ? document.height : document.body.offsetHeight;
	};

	//flag for drag/swipe
	document.addEventListener('touchmove', function(e){
		_TrueLYR.CONFIG.isTouchMove = true;
	});
	document.addEventListener('touchstart', function(e){
		if (event.originalEvent !== undefined && event.cancelable) {
			e.preventDefault();
		}
		_TrueLYR.CONFIG.isTouchMove = false;
	});

	//auto init
	const init = (function () {
		if( isTouchDevice() ){
			CONFIG.body.classList.add('touch');
		}
		if( detectIOS()){
			CONFIG.body.classList.add('ios');
		}
		CONFIG.IE = detectIE();
		if(CONFIG.IE !== false){
			CONFIG.body.classList.add('ie');
		}
	})();

	//pubic
	return {
		init: 					init,
		isInViewport: 			isInViewport,
		isTouchDevice: 			isTouchDevice,
		getViewportSize: 		getViewportSize,
		isInViewportY: 			isInViewportY,
		isBackInViewportY: 		isBackInViewportY,
		findAncestorByClass: 	findAncestorByClass,
		getComputedStyle: 		getComputedStyle,
		stripPx: 				stripPx,
		getRect: 				getRect,
		getOffset: 				getOffset,
		getElH: 				getElH,
		getElW: 				getElW,
		totalPageH: 			totalPageH,
		getTranslateValues: 	getTranslateValues,
		randomIntFromInterval:	randomIntFromInterval,
		isHidden: 				isHidden
	};
})(_TrueLYR);