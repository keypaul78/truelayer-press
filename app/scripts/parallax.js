/*
** ========================================================================
**  PARALLAX
** ========================================================================
	Active parallax on element di data-parallax attribute
	Set speed as value of data attribute

	ex <div data-parallax="0.3">

	Values can be positive and negative
** ========================================================================
*/

_TrueLYR.parallax = (function(_TrueLYR){
	'use strict';
	/*
		CONSTANTS
	*/
	const CONFIG = {
	selector :        '[data-parallax]',
	parallaxElms:     null,
	parallaxSpeed:    0.25,
	ticking:          false
	};

	/*
		METHODS
	*/
	//Updates Elements
	const update = function() {
		let i = 0;
		for (i = CONFIG.parallaxElms.length; i--;) {
		  const el = CONFIG.parallaxElms[i];
		  const parallaxSpeed = getParallaxSpeed(el);
		  const posElY = el.getBoundingClientRect().top;
		  const pageScrollY = document.documentElement.scrollTop || document.body.scrollTop;
		  const parallaxY =  posElY * parallaxSpeed ;
		  el.style.transform = 'translateX(0) translateY(' + parallaxY + 'px) translateZ(0)';
		}
	};

	//Get speed if setted or use config
	const getParallaxSpeed = function(el){
		let speed = Number(el.dataset.parallax) ? Number(el.dataset.parallax) : CONFIG.parallaxSpeed;
		return speed;
	}

	/*
		AUTO INIT
	*/
	const init = (function () {
		CONFIG.parallaxElms = document.querySelectorAll(CONFIG.selector);
	})();

	/*
		METHOD GOES PUBLIC
	*/
	return {
		update: update
	};

})(_TrueLYR);
