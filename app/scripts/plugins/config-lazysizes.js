/*
** ========================================================================
**  General Lazysize config
** ========================================================================
	
	More abuot lazysize
	https://github.com/aFarkas/lazysizes
	Nice lazyloader by (Alexander Farkas) aFarkas

** ========================================================================
*/

window.lazySizesConfig = window.lazySizesConfig || {};
lazySizesConfig.loadMode = 2;
lazySizesConfig.throttle = 600;
window.addEventListener('load', function(){
    lazySizesConfig.throttle = 150;
});
window.lazySizesConfig.customMedia = {
	'--xsmall': '(max-width: 575px)',
    '--small': '(max-width: 767px)',
    '--medium': '(max-width: 991px)',
    '--large': '(max-width: 1599px)',
    '--wider': '(min-width: 1600px)'
};

