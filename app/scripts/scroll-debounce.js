/*
** ========================================================================
**  Scroll debounce
** ========================================================================
	Scroll event request using RAF
** ========================================================================
*/
_TrueLYR.scroll = (function (_TrueLYR){
	'use strict';
	/*
		CONSTANTS
	*/
	const CONFIG = {
		latestKnownScrollY: 0,
		ticking: false,
		scrollPercetage: null,
		scrollDir: null
	};

	/*
		METHODS
	*/
	//General Updates
	const updateScroll = function() {
		CONFIG.ticking = false;
		CONFIG.latestKnownScrollY = setScrollPosition();
		let currentScrollY = CONFIG.latestKnownScrollY;
		if(_TrueLYR.parallax){
			_TrueLYR.parallax.update();
		}
	};

	//get current scroll
	const getCurrentScroll = function(){
		return CONFIG.latestKnownScrollY;
	}

	//flag/debounce
	const requestTickScroll = function() {
		if(!CONFIG.ticking) {
			requestAnimationFrame(updateScroll);
		}
		CONFIG.ticking = true;
	};

	//set scroll pos
	const setScrollPosition = function(){
		return window.scrollY ||
		document.body.scrollTop ||
		document.documentElement.scrollTop || 0;
	};

	//scroll in %
	const scrollPercetage = function(){
		var h = document.documentElement, 
        b = document.body,
        st = 'scrollTop',
        sh = 'scrollHeight';
    	return (h[st]||b[st]) / ((h[sh]||b[sh]) - h.clientHeight) * 100;
	};

	/*
		AUTO INIT
	*/
	const init = (function(){
		window.addEventListener('scroll', requestTickScroll);
		requestTickScroll();
	})();

	/*
		METHOD GOES PUBLIC
	*/
	return {
		init: init,
		getCurrentScroll: getCurrentScroll,
		scrollPercetage: scrollPercetage,
		updateScroll: updateScroll
	};

})(_TrueLYR);
