// generated on 2020-04-06 using generator-webapp 4.0.0-8
const { src, dest, watch, series, parallel, lastRun } = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync');
const del = require('del');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const { argv } = require('yargs');
const data = require('gulp-data');
const rename = require("gulp-rename");
const fs = require('fs');
const replace = require('gulp-replace');

const package = require('./package.json');

//server / env
const $ = gulpLoadPlugins();
const server = browserSync.create();
const port = argv.port || 9000;
let isProd = process.env.NODE_ENV === 'prod';

const envArgs = process.argv.slice(3);
isProd = envArgs[0] === '--prod' ? true : false;

const isTest = process.env.NODE_ENV === 'test';
const isDev = !isProd && !isTest;

//version
const isVersioned = false;
const lang = package.lang;
const version = package.version;
const replaceVersion = isVersioned ? '-' + lang + '_' + version : '';
const localePath = './app/locale/'+ lang +'.json';
const prodUrl = '';
const assetsPath = 'assets/';
const scriptsPath = 'scripts/';
const stylesPath = 'styles/';

const sassPaths = ['./node_modules'];

function views() {
  return src('app/*.pug')
    .pipe($.plumber())
    .pipe(data(function(file) {
      return JSON.parse(fs.readFileSync(localePath));
    }))
    .pipe($.pug(
      { pretty: true,
        basedir: 'app'
      }
    ))
    .pipe(dest('.tmp'))
    .pipe(server.reload({stream: true}));
};

function styles() {
  return src('app/styles/*.scss', {
    sourcemaps: !isProd,
  })
    .pipe($.plumber())
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: [sassPaths]
    }).on('error', $.sass.logError))
    .pipe($.postcss([
      autoprefixer()
    ]))
    .pipe(dest('.tmp/styles', {
      sourcemaps: !isProd,
    }))
    .pipe(server.reload({stream: true}));
};

function scripts() {
  return src('app/scripts/**/*.js', {
    sourcemaps: !isProd,
  })
    .pipe($.plumber())
    .pipe($.babel())
    .pipe(dest('.tmp/scripts', {
      sourcemaps: !isProd ? '.' : false,
    }))
    .pipe(server.reload({stream: true}));
};


const lintBase = (files, options) => {
  return src(files)
    .pipe($.eslint(options))
    .pipe(server.reload({stream: true, once: true}))
    .pipe($.eslint.format())
    .pipe($.if(!server.active, $.eslint.failAfterError()));
}
function lint() {
  return lintBase('app/scripts/**/*.js', { fix: true })
    .pipe(dest('app/scripts'));
};
function lintTest() {
  return lintBase('test/spec/**/*.js');
};

function html() {
  return src(['app/*.html', '.tmp/*.html'])
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    .pipe($.if(/\.js$/, $.uglify()))
    .pipe($.if(/\.js$/, rename({suffix: replaceVersion})))
    .pipe($.if(/\.css$/,$.postcss([cssnano({safe: true, autoprefixer: false})])))
    .pipe($.if(/\.css$/,rename({suffix: replaceVersion})))
    .pipe($.if(/\.html$/, 
        $.htmlmin({
            collapseWhitespace: true,
            minifyCSS: true,
            minifyJS: {compress: {drop_console: true}},
            processConditionalComments: true,
            removeComments: true,
            removeEmptyAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true
        })
    ))
    .pipe(
      replace(/href=\"(\S*)\.css\"/gi, 'href="$1'+ replaceVersion +'.css"'),
    )
    .pipe(
      replace(/src=\"(\S*)\.js\"/gi, 'src="$1'+ replaceVersion +'.js"')
    )
    .pipe(dest('dist'));
}

function assets() {
  return src('app/assets/**/*', { since: lastRun(assets) })
    //.pipe($.imagemin())
    .pipe(dest('dist/assets'));
};


function extras() {
  return src([
    'app/*',
    '!app/*.html',
    '!app/views',
    '!app/blocks',
    '!app/mixins',
    '!app/locale',
    '!app/*.pug'
  ], {
    dot: true
  }).pipe(dest('dist'));
};

function clean() {
  return del(['.tmp', 'dist'])
}

function measureSize() {
  return src('dist/**/*')
    .pipe($.size({title: 'build', gzip: true}));
}

const build = series(
  clean,
  parallel(
    lint,
    series(parallel(views, styles, scripts), html),
    assets,
    extras
  ),
  measureSize
);


const prod = series(
  clean,
 parallel(
    lint,
    series(parallel(views, styles, scripts), html),
    assets
  ),
  extras
);

function startAppServer() {
  server.init({
    notify: false,
    port,
    server: {
      baseDir: ['.tmp', 'app'],
      routes: {
        '/node_modules': 'node_modules'
      }
    }
  });

  watch([
    'app/*.html',
    'app/assets/**/*'
  ]).on('change', server.reload);

  watch('app/**/*.pug', views);
  watch('app/styles/**/*.scss', styles);
  watch('app/scripts/**/*.js', scripts);
  watch('app/locale/*.json', views)
}

function startTestServer() {
  server.init({
    notify: false,
    port,
    ui: false,
    server: {
      baseDir: 'test',
      routes: {
        '/scripts': '.tmp/scripts',
        '/node_modules': 'node_modules'
      }
    }
  });

  watch('test/index.html').on('change', server.reload);
  watch('app/scripts/**/*.js', scripts);
  watch('test/spec/**/*.js', lintTest);
}

function startDistServer() {
  server.init({
    notify: false,
    port,
    server: {
      baseDir: 'dist',
      routes: {
        '/node_modules': 'node_modules'
      }
    }
  });
}

let serve;
if (isDev) {
  serve = series(clean, parallel(views, styles, scripts), startAppServer);
} else if (isTest) {
  serve = series(clean, scripts, startTestServer);
} else if (isProd) {
  serve = series(build, startDistServer);
}

exports.serve = serve;
exports.build = build;
exports.prod = prod;
exports.default = build;
